export default {
    collection: [],
    selectedArtist: { artists: [], tracks: [], covers: {} },
    page: 1,
    loading: false,
    eos: false
}