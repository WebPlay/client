export default {
    toggleShuffleMode(state) {
        state.shuffle = !state.shuffle;
    },
    requestReplayTrack(state) {
        state.requestReplayTrack = !state.requestReplayTrack;
    }
}