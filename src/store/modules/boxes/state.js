export default {
    collection: [],
    newest: [],
    selectedBox: { videos: [], covers: {} },
    page: 1,
    loading: false,
    eos: false
}