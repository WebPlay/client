export default {
    collection(state) {
        return state.collection;
    },
    selectedRadio(state) {
        return state.selectedRadio;
    }
}