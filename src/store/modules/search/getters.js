export default {
    collection(state) {
        return state.collection;
    },
    term(state){
        return state.term;
    }
}