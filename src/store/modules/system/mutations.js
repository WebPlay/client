export default {
    setUsers(state, users) {
        state.users = users;
    },
    setRoles(state, roles) {
        state.roles = roles;
    }
}