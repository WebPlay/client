export default {
    collection: [],
    newest: [],
    selectedAlbum: { tracks: [], covers: {} },
    page: 1,
    loading: false,
    eos: false
}