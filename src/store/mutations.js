export default {
  resetViewMenu(state) {
    state.viewMenu = [];
  },
  searchFilter(state, term) {
    state.searchFilter = term;
  },
  setServer(state, server) {
    state.server = server;
  },
  setSystemSettings(state, payload) {
    state.serverConfig.allows = payload;
  },
  setViewMenu(state, menuItems) {
    state.viewMenu = menuItems || [];
  },
  setServerConfig(state, config) {
    state.serverConfig = config;
  },
  setServerConfigAllows(state, allows) {
    state.serverConfig.allows = allows;
  },
  setSystemConfigDomains(state, domains) {
    state.serverConfig.domains = domains;
  },
  setServerInfo(state, info) {
    state.serverInfo = info;
  }
}