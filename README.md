# WebPlay Client
[![Build Status](https://drone.anufrij.de/api/badges/WebPlay/client/status.svg)](https://drone.anufrij.de/WebPlay/client)

For elementary OS users I created a similar native application written in Vala: https://github.com/artemanufrij/playmymusic

Web client for [WebPlay Server](/WebPlay/server)

## Screenshots
<p align="center">
    <img src="screenshots/mobile_home.png" width="20%"/>
    <img src="screenshots/mobile_album.png" width="20%"/>
    <img src="screenshots/mobile_artist.png" width="20%"/>
    <img src="screenshots/mobile_boxes.png" width="20%"/>
</p>

![Albums](screenshots/home.png)

![Albums](screenshots/albums.png)

![Artists](screenshots/artists.png)

![Videos](screenshots/videos.png)

## How to install your onw instance
[Documentation](/WebPlay/docker#requirements)

### Requirements
``` bash
# Debian based
sudo apt install git nodejs

# Arch based
sudo pacman -S git nodejs
```

### Clone Repository
``` bash
# clone repository
git clone https://gitea.com/WebPlay/client.git webplay-client

# move into directory
cd webplay-client
```

### Build Setup
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build
```

### Build an Electron App (optional)
``` bash
# package into an AppImage
npm run linux-appimage
```

## Support
Join our Matrix room: <a href="https://matrix.to/#/#WebPlay:matrix.anufrij.de">#WebPlay:matrix.anufrij.de</a>

## Donate
<a href="https://www.paypal.me/ArtemAnufrij">PayPal</a> | <a href="https://liberapay.com/Artem/donate">LiberaPay</a> | <a href="https://www.patreon.com/ArtemAnufrij">Patreon</a>